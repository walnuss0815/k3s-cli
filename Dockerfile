FROM debian:11.5-slim

RUN apt-get update \
 && apt-get install -y --no-install-recommends ca-certificates git curl bash bash-completion vim nano \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN curl -s -L -o /usr/bin/k3sup https://github.com/alexellis/k3sup/releases/download/0.12.3/k3sup \
 && chmod +x /usr/bin/k3sup

RUN curl -s -L -o /usr/bin/kubectl https://dl.k8s.io/release/v1.25.0/bin/linux/amd64/kubectl \
 && chmod +x /usr/bin/kubectl

RUN curl -s -L https://github.com/derailed/k9s/releases/download/v0.26.6/k9s_Linux_x86_64.tar.gz | tar xz -C /usr/bin/ \
 && chmod +x /usr/bin/k9s

RUN curl -s -L https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv4.5.7/kustomize_v4.5.7_linux_amd64.tar.gz | tar xz -C /usr/bin/ \
 && chmod +x /usr/bin/kustomize

RUN curl -s -L https://get.helm.sh/helm-v3.10.0-linux-amd64.tar.gz | tar xz --strip-components=1 -C /usr/bin/ linux-amd64/helm \
 && chmod +x /usr/bin/helm

RUN curl -s -L -o /usr/bin/kubectl https://dl.k8s.io/release/v1.25.0/bin/linux/amd64/kubectl \
 && chmod +x /usr/bin/kubectl

RUN curl -s -L -o /usr/bin/kubectx https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubectx \
 && chmod +x /usr/bin/kubectx

RUN curl -s -L -o /usr/bin/kubens https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens \
 && chmod +x /usr/bin/kubens

RUN curl -s -L https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.18.5/kubeseal-0.18.5-linux-amd64.tar.gz | tar xz -C /usr/bin/  \
 && chmod +x /usr/bin/kubeseal

RUN curl -s -L -o /usr/bin/kube-ps1.sh https://raw.githubusercontent.com/jonmosco/kube-ps1/master/kube-ps1.sh \
 && chmod +x /usr/bin/kube-ps1.sh

COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

COPY init.sh /etc/profile.d/
RUN chmod +x /etc/profile.d/init.sh

RUN echo "source /etc/profile" >> $HOME/.bashrc \
 && chmod +x $HOME/.bashrc
