# k3s CLI

## Build
```bash
docker build -t k3s-cli .
```

## Run
```bash
docker-compose -f /path/to/k3s-cli/docker-compose.yml run -v /path/to/repos:/repos k3s-cli
```
