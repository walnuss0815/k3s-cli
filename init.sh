#!/bin/bash

## Kubectl
source <(kubectl completion bash)
alias k=kubectl
complete -o default -F __start_kubectl k

## Kustomize
source <(kustomize completion bash)

## Helm
source <(helm completion bash)

## Shell
source /usr/bin/kube-ps1.sh
export PS1='[\u@\h \W $(kube_ps1)]\$ '
